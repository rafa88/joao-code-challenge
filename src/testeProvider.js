import Provider from './ProviderFactory'
import getConfig from '../Config'
import ConfigRouteParser from '../Helpers/ConfigRouteParser'

const BaseProvider = new Provider()

/**
 * Creates request to list bases
 * @returns {ProviderRequest} providerRequest
 */
export const listBases = (params) => {
    const routeConfig = ConfigRouteParser(
        getConfig().routes.base.list,
    )
    return BaseProvider.createRequest({
        method: routeConfig.method,
        url: routeConfig.url,
        params: params
    })
}

export const createBase = (baseData) => {
    const routeConfig = ConfigRouteParser(getConfig().routes.base.create)
    return BaseProvider.createRequest({
        method: routeConfig.method,
        url: routeConfig.url,
        headers: { 'Content-Type': 'multipart/form-data' },
        data: baseData
    })
}

export const getBaseDetails = (baseId) => {
    return BaseProvider.createRequest(
        ConfigRouteParser(
            getConfig().routes.base.details,
            { baseId }
        )
    )
}

export const deleteBase = (baseId) => {
    return BaseProvider.createRequest(
        ConfigRouteParser(
            getConfig().routes.base.delete,
            { baseId }
        )
    )
}

export const editBase = ({ baseId, baseData }) => {
    const routeConfig = ConfigRouteParser(getConfig().routes.base.update, { baseId })
    return BaseProvider.createRequest({
        method: routeConfig.method,
        url: routeConfig.url,
        data: baseData
    })
}
