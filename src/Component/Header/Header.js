import React from 'react';
import { connect } from 'react-redux';

import './Header.css';

const Header = ({address, total}) => {
  return (
    <header className="header display-flex align-items-center">
      {(address.address) &&
        <div className="center-page display-flex justify-content-flex-end header-address">
          <p>
            Endereço de entrega<br />
            <strong>{address.address.address1}, {address.address.number}</strong>
          </p>
          <p>
            Total:<br />
            <strong>{total.toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' })}</strong>
          </p>
        </div>
      }
      {(!address.address) &&
        <div className="center-page display-flex justify-content-flex-end header-user">
          <button className="header-user-btn">login</button>
        </div>
      }
    </header>
  )
}

const mapStateToProps = (store) => ({
  address: store.address,
  total: store.cart,
})

const HeaderController = connect(
  mapStateToProps,
  null
)(Header)
export default HeaderController;

