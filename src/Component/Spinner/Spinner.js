import React from 'react';

import './Spinner.css';

class Spinner extends React.Component {
  render() {

    return (
      <div className="lds-roller display-flex align-items-center justify-content-center">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    );
  }
}

export default Spinner
