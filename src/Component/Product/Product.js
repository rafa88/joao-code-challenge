import React from 'react';
import logo from '../../images/cerveja.jpg';

import './Product.css';
class Product extends React.Component {

  state = {
    amount: 0
  }

  add = (price) => {
    const count = this.state.amount + 1;
    this.props.addProducts(price);
    this.setState({amount: count});
  }

  remove = (price) => {
    if (this.state.amount === 0) return false;
    const count = this.state.amount - 1;
    this.props.removeProducts(price);
    this.setState({ amount: count });
  }
  
  render() {
    const { product } = this.props;
    return (
      <div className="product">
        <p className="product-title">{product.productVariants[0].title}</p>
        <img src={logo} className="product-image" alt="logo" />
        <p className="product-price">{product.productVariants[0].price.toLocaleString('pt-BR', { minimumFractionDigits: 2, style: 'currency', currency: 'BRL' })}</p>
    
        <div className="product-amount">
          <button onClick={() => this.remove(product.productVariants[0].price)} className="product-amount-btn red-btn display-flex justify-content-center align-items-center">
            -
          </button>
          <div className="product-amount-input">
            <p className="text-input">{this.state.amount === 0 ? '' : this.state.amount}</p>
          </div>
          <button onClick={() => this.add(product.productVariants[0].price)} className="product-amount-btn blue-btn display-flex justify-content-center align-items-center">
            +
          </button>
        </div>
      </div>
    );
  }
}

export default Product;
