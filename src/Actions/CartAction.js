import { ADD_PROD, REMOVE_PROD } from "./ActionTypes";

export const addProducts = (price) => async (dispatch, getState) => {
  const value = getState().cart;
  dispatch({ type: ADD_PROD, value: value + price });
};

export const removeProducts = (price) => async (dispatch, getState) => {
  const value = getState().cart;
  dispatch({ type: REMOVE_PROD, value: value - price });
};
