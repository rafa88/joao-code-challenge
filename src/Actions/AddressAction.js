import { FETCH_POC, FETCH_POC_RECEIVE, FETCH_POC_ERROR } from "./ActionTypes";
import Axios from "axios";

export const fetchPoc = (lat, lng) => async (dispatch, getState) => {
  dispatch({ type: FETCH_POC });

  Axios({
    url: 'https://api.code-challenge.ze.delivery/public/graphql',
    method: 'post',
    data: {
      variables: {
        "algorithm": "NEAREST",
        "lat": lat,
        "long": lng,
        "now": new Date()
      },
      query: `query pocSearchMethod($now: DateTime!, $algorithm: String!, $lat: String!, $long: String!) {
          pocSearch(now: $now, algorithm: $algorithm, lat: $lat, long: $long) {
            __typename
            id
            status
            tradingName
            officialName
            deliveryTypes {
              __typename
              pocDeliveryTypeId
              deliveryTypeId
              price
              title
              subtitle
              active
            }
            paymentMethods {
              __typename
              pocPaymentMethodId
              paymentMethodId
              active
              title
              subtitle
            }
            pocWorkDay {
              __typename
              weekDay
              active
              workingInterval {
                __typename
                openingTime
                closingTime
              }
            }
            address {
              __typename
              address1
              address2
              number
              city
              province
              zip
              coordinates
            }
            phone {
              __typename
              phoneNumber
            }
          }
        }`
    }
  }).then((result) => {
    sessionStorage.setItem('pocSearch', result.data.data.pocSearch[0].id);
    sessionStorage.setItem('address', JSON.stringify(result.data.data.pocSearch[0].address));
    sessionStorage.setItem('lat', lat);
    sessionStorage.setItem('lng', lng);
    dispatch({
      type: FETCH_POC_RECEIVE,
      value: {
        pocSearch: result.data.data.pocSearch[0],
        lat: lat,
        lng: lng
      }
    });
  }).catch((error) => dispatch({ type: FETCH_POC_ERROR }));

};
