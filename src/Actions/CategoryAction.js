import { FETCH_CATEGORY, FETCH_CATEGORY_RECEIVE, FETCH_CATEGORY_ERROR } from "./ActionTypes";
import Axios from "axios";

export const fetchCategory = (lat, lng) => async (dispatch, getState) => {
  dispatch({ type: FETCH_CATEGORY });

  Axios({
    url: 'https://api.code-challenge.ze.delivery/public/graphql',
    method: 'post',
    data: {
      variables: {
        "algorithm": "NEAREST",
        "lat": lat,
        "long": lng,
        "now": new Date()
      },
      query: `query allCategoriesSearch {
        allCategory{
          title
          id
        }
      }`
    }
  }).then((result) => {
    dispatch({
      type: FETCH_CATEGORY_RECEIVE,
      value: result.data.data.allCategory
    });
  }).catch((error) => dispatch({ type: FETCH_CATEGORY_ERROR }));

};
