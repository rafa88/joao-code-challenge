import { FETCH_PRODUTCS, FETCH_PRODUTCS_RECEIVE, FETCH_PRODUTCS_ERROR } from "./ActionTypes";
import Axios from "axios";

export const fetchProducts = (variables) => async (dispatch, getState) => {
  dispatch({ type: FETCH_PRODUTCS });

  Axios({
    url: 'https://api.code-challenge.ze.delivery/public/graphql',
    method: 'post',
    data: {
      variables: variables,
      query: `query poc($id: ID!, $categoryId: Int, $search: String){
        poc(id: $id) {
          id
          products(categoryId: $categoryId, search: $search) {
            id
            title
            rgb
            images {
              url
            }
            productVariants {
              availableDate
              productVariantId
              price
              inventoryItemId
              shortDescription
              title
              published
              volume
              volumeUnit
              description
              subtitle
              components {
                id
                productVariantId
                productVariant {
                  id
                  title
                  description
                  shortDescription
                }
              }
            }
          }
        }
      }`
    }
  }).then((result) => {
    dispatch({
      type: FETCH_PRODUTCS_RECEIVE,
      value: {
        products: result.data.data.poc.products,
        poc: result.data.data.poc.id,
      }
    });
  }).catch((error) => dispatch({ type: FETCH_PRODUTCS_ERROR }));

};
