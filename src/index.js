import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route  } from 'react-router-dom'
import { Provider } from 'react-redux';

import * as serviceWorker from './serviceWorker';

import Home from './Screen/Home/Home';
import Products from './Screen/Products/Products';
import { store } from './store';

import './index.css';

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <Switch>
        <Route path="/" exact={true} component={Home} />
        <Route path="/produtos" component={Products} />
      </Switch>
    </Provider>
  </BrowserRouter>, document.getElementById('root'));

serviceWorker.unregister();
