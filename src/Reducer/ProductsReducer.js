import { FETCH_PRODUTCS, FETCH_PRODUTCS_ERROR, FETCH_PRODUTCS_RECEIVE } from "../Actions/ActionTypes";

const initialState = {
  products: [],
  poc: null,
  isLoading: false,
};

export const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUTCS:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_PRODUTCS_ERROR:
      return {
        ...state,
        isLoading: false,
        products: [],
        poc: null
      };
    case FETCH_PRODUTCS_RECEIVE:
      return {
        ...state,
        isLoading: false,
        products: action.value.products,
        poc: action.value.poc
      };
    default:
      return state;
  }
};
