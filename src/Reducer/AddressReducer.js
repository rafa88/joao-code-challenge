import { FETCH_POC, FETCH_POC_ERROR, FETCH_POC_RECEIVE } from "../Actions/ActionTypes";

const initialState = {
  address: JSON.parse(sessionStorage.getItem('address')) || null,
  lat: sessionStorage.getItem('lat') || null,
  lng: sessionStorage.getItem('lng') || null,
  poc: sessionStorage.getItem('pocSearch') || null,
  isLoading: false,
};

export const addressReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POC:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_POC_ERROR:
      return {
        ...state,
        isLoading: false,
        address: null,
        lat: null,
        lng: null,
        poc: null
      };
    case FETCH_POC_RECEIVE:
      return {
        ...state,
        isLoading: false,
        address: action.value.pocSearch.address,
        lat: action.value.lat,
        lng: action.value.lng,
        poc: action.value.pocSearch.id
      };
    default:
      return state;
  }
};
