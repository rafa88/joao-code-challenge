import { FETCH_CATEGORY, FETCH_CATEGORY_ERROR, FETCH_CATEGORY_RECEIVE } from "../Actions/ActionTypes";

const initialState = {
  categories: [],
  isLoading: false,
};

export const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORY:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_CATEGORY_ERROR:
      return {
        ...state,
        isLoading: false,
        categories: [],
      };
    case FETCH_CATEGORY_RECEIVE:
      return {
        ...state,
        isLoading: false,
        categories: action.value,
      };
    default:
      return state;
  }
};
