import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";

import lupa from '../../images/lupa.svg';
import './Home.css';

import Header from '../../Component/Header/Header';
import { fetchPoc } from '../../Actions/AddressAction';
import Spinner from '../../Component/Spinner/Spinner';

const google = window.google

class Home extends React.Component {

  constructor(props) {
    super(props);
    this.autocompleteInput = React.createRef();
    this.autocomplete = null;
    this.handlePlaceChanged = this.handlePlaceChanged.bind(this);
  }

  componentDidMount() {
    this.autocomplete = new google.maps.places.Autocomplete(this.autocompleteInput.current,
      { "types": ["geocode"] });

    this.autocomplete.addListener('place_changed', this.handlePlaceChanged);
  }

  handlePlaceChanged() {
    const { fetchPoc, history } = this.props;
    const place = this.autocomplete.getPlace();
    fetchPoc(place.geometry.location.lat(), place.geometry.location.lng());
    history.push("produtos");
  }

  render() {
    const { loading } = this.props;

    return (
      <div className="App">
        {(loading) && <Spinner />}
        <Header />

        <section className="search-address">
          <input type="text" className="text-input" ref={this.autocompleteInput} id="autocomplete" placeholder="Ex. Rua Américo Brasiliense, São Paulo" />
          <button>
            <img src={lupa} className="search-address-image" alt="buscar endereço" />
          </button>
        </section>
      </div>
    );
  }
}

const mapStateToProps = (store) => ({
  address: store.address,
  loading: store.address.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  fetchPoc: (lat, lng) => { dispatch(fetchPoc(lat, lng)) },
});

const HomeController = connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
export default withRouter(HomeController)
