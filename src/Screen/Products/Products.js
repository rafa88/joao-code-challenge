import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import { addProducts, removeProducts } from '../../Actions/CartAction';
import { fetchProducts } from '../../Actions/ProductsAction';

import './Products.css';
import lupa from '../../images/lupa.svg';

import Header from '../../Component/Header/Header';
import Product from '../../Component/Product/Product';
import { fetchCategory } from '../../Actions/CategoryAction';
import Spinner from '../../Component/Spinner/Spinner';

class Products extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      category: '',
      search: ''
    }
  }
  
  componentDidMount() {
    const { pocSearch, fetchProducts, fetchCategory, address } = this.props;
    fetchProducts({ "id": pocSearch, "search": "", "categoryId": null });
    fetchCategory(address.lat, address.lng);
  }
  
  changeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value})
  }

  onKeyUp = (e) => {
    const { fetchProducts, pocSearch } = this.props;
    if (e.target.value === '') {
      fetchProducts({ "id": pocSearch, "search": null, "categoryId": null });
    }
  }

  selectCategory = (e) => {
    const { fetchProducts, pocSearch } = this.props;
    const category = e.target.value || null;
    fetchProducts({ "id": pocSearch, "search": null, "categoryId": category });
  }

  onSearch = (e) => {
    e.preventDefault();
    const { fetchProducts, pocSearch } = this.props;
    if (this.state.search !== '') {
      fetchProducts({ "id": pocSearch, "search": this.state.search, "categoryId": null });
    }
  }

  render() {
    const { loading, products, addProducts, removeProducts, categories } = this.props;

    return (
      <div className="App">
        {(loading) && <Spinner />}
        <Header />

        <form onSubmit={this.onSearch} className="center-page box-filter display-flex justify-content-space-between product-filter">
          <select className="filter-select" name="category" value={this.state.category} onChange={(e) => {this.changeHandler(e); this.selectCategory(e)}}>
            <option value="">Todos</option>
            {categories.map(function (category, index) {
              return <option key={index} value={category.id}>{category.title}</option>
            })}
          </select>

          <section className="filter-input ">
            <input type="text" className="text-input" placeholder="Pesquisar" name="search" value={this.state.search} onChange={this.changeHandler} onKeyUp={this.onKeyUp} />
            <button>
              <img src={lupa} className="search-address-image" alt="Buscar" />
            </button>
          </section>
        </form>

        <section className="center-page products-list">
          {products.map(function (prod, index) {
            return <Product key={index} product={prod} addProducts={addProducts} removeProducts={removeProducts} />
          })}
        </section>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  address: state.address,
  pocSearch: state.address.poc,
  products: state.products.products,
  categories: state.categories.categories,
  loading: state.products.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  fetchCategory: (variables) => { dispatch(fetchCategory(variables)) },
  fetchProducts: (fetchCategory) => { dispatch(fetchProducts(fetchCategory)) },
  addProducts: (price) => { dispatch(addProducts(price)) },
  removeProducts: (price) => { dispatch(removeProducts(price)) },
});

const ProductsController = connect(
  mapStateToProps,
  mapDispatchToProps
)(Products)
export default withRouter(ProductsController)
